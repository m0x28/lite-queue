/****************************************************************************************************************//**
 * @file    liteq.h
 * @author  Maxim Ivanchenko
 * @date    June 2019
 * @brief   Light and simple queue. It's not thread-safe.
 *******************************************************************************************************************/
#ifndef __LITE_QUEUE_H__
#define __LITE_QUEUE_H__
/*******************************************************************************************************************/
// #define LITEQ_DEFAULT_NUM 		16
// #define LITEQ_DEFAULT_INCRSBL 	1
// #define LITEQ_DEFAULT_LIMIT 	1024

#define LITEQ_ERR_ENOMEM		(-2) /// Memory allocation for element failed
#define LITEQ_ERR_EMPTYQ		(-3) /// Queue is empty
/*******************************************************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/*******************************************************************************************************************/
/// Lite queue element
typedef struct sLiteqel tLiteqel;
struct sLiteqel
{
	tLiteqel* pNext;
	tLiteqel* pPrev;
	void* pData;
};

/*******************************************************************************************************************/
// /// Lite queue config
// typedef struct sLiteqconf
// {
// 	int num; ///< Number of elements in pool initially.
// 	int increasable; /// 0 - elements can not be increase if num reached (error will be returned), 1 - elements can be increase up to <limit>.
// 	int limit; ///< Upper limit of number of elements in pool.
// }
// tLiteqconf;

/*******************************************************************************************************************/
/// Lite queue entity object
typedef struct sLiteq
{
	int num; ///< Number of elements in queue currently
	// int increasable; /// 0 - elements can not be increase, 1 - elements can not be increase
	// int limit; ///< Upper limit of number of elements in pool.
	tLiteqel* pFirst; ///< A reference to the first (the newest) element
	tLiteqel* pLast; ///< A reference to the last (the oldest) element
}
tLiteq;

/****************************************************************************************************************//**
 * @brief  Initiate liteq entity.
 * @return 0 - ok. Other - error.
 *******************************************************************************************************************/
// int LiteqInit(tLiteqconf* pConf /** [in] Config. Can be NULL, so deafults will be used*/, tLiteq* pThis/** [in] liteq entity*/);
int LiteqInit(tLiteq* pThis/** [in] liteq entity*/);

/****************************************************************************************************************//**
 * @brief  Get number of elements in queue currently.
 * @return Number of elements in queue currently.
 *******************************************************************************************************************/
int LiteqNum(tLiteq* pThis /** [in] Queue object*/);

/****************************************************************************************************************//**
 * @brief  Add element to queue.
 * Memory allocation for element comes up every time when the method called.
 * @return Not negative - number of elements in queue after push. Nagative - error.
 *******************************************************************************************************************/
int LiteqPush(void* pData /** [in] Data to put*/,  tLiteq* pThis /** [in] Queue object*/);

/****************************************************************************************************************//**
 * @brief  Get element from queue.
 * Memory clean-up comes up every time when the method called.
 * @return Not negative - number of elements in queue after pop. Nagative - error.
 *******************************************************************************************************************/
int LiteqPop(void** ppData /** [in] Data to get. Can be NULL*/,  tLiteq* pThis /** [in] Queue object*/);

/*******************************************************************************************************************/
#ifdef __cplusplus
}
#endif
/*******************************************************************************************************************/
#endif // __LITE_QUEUE_H__
