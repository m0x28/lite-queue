/****************************************************************************************************************//**
 * @file    liteq.c
 * @author  Maxim Ivanchenko
 * @date    June 2019
 * @brief   Light and simple queue. It's not thread-safe.
 *******************************************************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "liteq.h"

/* Common  thoughts:
- It's better to check conditions, basing on the first/last pointers, than the number of elements in the queue,
because the number of elements it not mandatory and it can be deprecated, if required.  */

/****************************************************************************************************************//**
 * @brief  Initiate liteq entity. Pointer can be as static so dynamic.
 * @return 0 - ok. Other - error.
 *******************************************************************************************************************/
#if 0
int LiteqInit(tLiteqconf* pConf /** [in] Config. Can be NULL, so deafults will be used*/, tLiteq* pThis/** [in] liteq entity*/)
{
	if(pConf)
	{
		pThis->num = pConf->num;
		pThis->limit = pConf->limit;
		pThis->increasable = pConf->increasable;
	}
	else
	{
		pThis->num = LITEQ_DEFAULT_NUM;
		pThis->limit = LITEQ_DEFAULT_LIMIT;
		pThis->increasable = LITEQ_DEFAULT_INCRSBL;
	}
	
	malloc(pThis->num * )
	pThis->pNext = NULL;
}
#else
int LiteqInit(tLiteq* pThis/** [in] liteq entity*/)
{
	memset(pThis, 0, sizeof(tLiteq));
	return 0;
}
#endif

/****************************************************************************************************************//**
 * @brief  Get number of elements in queue currently.
 * @return Number of elements in queue currently.
 *******************************************************************************************************************/
int LiteqNum(tLiteq* pThis /** [in] Queue object*/)
{
	return pThis->num;
}

/****************************************************************************************************************//**
 * @brief  Add element to queue.
 * Memory allocation for element comes up every time when the method called.
 * @return Positive - number of elements in queue after push. Nagative - error.
 *******************************************************************************************************************/
int LiteqPush(void* pData /** [in] Data to put*/,  tLiteq* pThis /** [in] Queue object*/)
{
	// Allocate a new element
	tLiteqel* pElement = malloc(sizeof(tLiteqel));
	if(!pElement)
	{
		return LITEQ_ERR_ENOMEM;
	}
	// Check for max elements restriction here
	// ...

	// Setup links in the element
	pElement->pPrev = pThis->pLast;
	pElement->pNext = NULL;
	pElement->pData = pData;

	// Update link in the neighbour element
	if(pThis->pLast)
	{
		pThis->pLast->pNext = pElement;
	}
	
	// Update links in the queue
	pThis->pLast = pElement;
	if(pThis->pFirst == NULL)
	{
		pThis->pFirst = pElement; // This happens only if the queue was empty before pushing
	}
	++pThis->num;
	return pThis->num;
}

/****************************************************************************************************************//**
 * @brief  Get element from queue.
 * Memory clean-up comes up every time when the method called.
 * @return Not negative - number of elements in queue after pop. Nagative - error.
 *******************************************************************************************************************/
int LiteqPop(void** ppData /** [in] Data to get. Can be NULL*/,  tLiteq* pThis /** [in] Queue object*/)
{
	int retInt = LITEQ_ERR_EMPTYQ;
	if(pThis->pFirst)
	{
		// Extract data
		tLiteqel* pNext = pThis->pFirst->pNext;
		if(ppData)
		{
			*ppData = pThis->pFirst->pData;
			free(pThis->pFirst);
		}

		// Update link in the queue and in the neighbor element
		pThis->pFirst = pNext;
		if(pNext)
		{
			pNext->pPrev = NULL;
		}
		else
		{
			pThis->pLast = NULL;			
		}
		
		--pThis->num;
		retInt = pThis->num;
	}
	
	return retInt;
}

/*******************************************************************************************************************/
