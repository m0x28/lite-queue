/****************************************************************************************************************//**
 * @file    liteq-test.c
 * @author  Maxim Ivanchenko
 * @date    June 2019
 * @brief   Test for liteq.
 * Compiling:   $ gcc liteq.c liteq-test.c -DLITEQ_TEST
 * Testing:     $ valgrind ./a.out 200
 *              $ time valgrind ./a.out 1000000 > /dev/null
 *******************************************************************************************************************/
#ifdef LITEQ_TEST

#include <stdlib.h> // EXIT_SUCCESS, rand, malloc, strtol
#include <stdio.h>
#include <unistd.h> // sleep 
#include <time.h>
#include <errno.h> 
#include <limits.h>  // LONG_MAX, LONG_MIN
#include "liteq.h"

#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"
/*******************************************************************************************************************/
static tLiteq gQ;

static void Push()
{
    static int a = 1;
    static int b = 1;
    int* p = malloc(sizeof(int) * 4);
    if(p)
    {
        p[0] = a;
        p[1] = b;
        int resInt = LiteqPush(p, &gQ);
        if(resInt > 0)
        {
            printf(ANSI_COLOR_YELLOW"> [%d]\t%08X %08X\n", resInt, a, b);
            a = a == 0 ? 1 : a << 1;
            ++b;
        }
        else
        {
            printf(ANSI_COLOR_YELLOW"> error %d\n", resInt);
        }
        free(p);
    }
    else
    {
        printf(ANSI_COLOR_YELLOW"> malloc error\n");
    }
}
/*******************************************************************************************************************/
static void Pop()
{
    int* p = NULL;
    int resInt = LiteqPop((void**)&p, &gQ);
    if(resInt >= 0)
    {
        printf(ANSI_COLOR_BLUE"< [%d]\t%08X %08X \n", resInt, p[0], p[1]);
        free(p);
    }
    else if(resInt == LITEQ_ERR_EMPTYQ)
    {   
        printf(ANSI_COLOR_BLUE"< [0]\talready empty\n");
    }
    else
    {
        printf(ANSI_COLOR_BLUE"< error %d\n", resInt);
    }
}
/*******************************************************************************************************************/
static void Flush()
{
    int n = LiteqNum(&gQ);
    for (int i = 0; i < n; i++)
    {
        Pop();
    }
}    
/*******************************************************************************************************************/
int main(int argc, char* argv[])
{
    int iterNum = 1000; 
    if(argc > 1)
    {
        errno = 0;
        long int arg = strtol(argv[1], NULL, 10);
        if ((errno == ERANGE && (arg == LONG_MAX || arg == LONG_MIN)) || (errno != 0 && arg == 0))
        {
            perror("liteq-test: strtol");
            return EXIT_FAILURE;
        }
        iterNum = (int)arg;
    }

    if(LiteqInit(&gQ))
    {
        return EXIT_FAILURE;
    }

    printf("The test of \"liteq\".\n"
    "You may enter a number of iteration as the argument of the current executable.\n"
    "Log description: Direction(Push/Pop) [Elements in queue currently]  Element's data(Shifter and Counter)\n");
    
    srand((unsigned int)(time(NULL)));
    while(iterNum--)
    {
        int r = rand();
        int repeat = (r&0xF) < 4 ? r&3 : 1;
        if(r&1)
        {   
            while(repeat--) Push();
        }
        else
        {
            while(repeat--) Pop();
        }

        if(LiteqNum(&gQ) > 15)
        {
            Flush();
        }
    }
    
    Flush();
    printf(ANSI_COLOR_RESET);
    return EXIT_SUCCESS;
}

#endif // LITEQ_TEST
/*******************************************************************************************************************/
